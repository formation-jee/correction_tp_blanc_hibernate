<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 08/02/2021
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Listing des utilisateurs</h1>

<h2>Etudiants de l'année ${year}</h2>
<ul>
    <c:forEach var="student" items="${requestScope.etudiantsYear}">
        <li>${student.nom} - ${student.prenom}</li>
    </c:forEach>
</ul>


<h2>${requestScope.nbEtudiants} en BDD</h2>

<h3>Les criteres de la matière Angular :</h3>
<ul>
<c:forEach var="critere" items="${requestScope.criteres}">
    <li>${critere.description} - ${critere.pointsReparti}</li>
</c:forEach>
</ul>

<div id="global" class="container">
    <div class="row">
        <h3>Les utilisateurs en BDD :</h3>
        <ul>
            <c:forEach var="utilisateur" items="${requestScope.utilisateurs}">

                <li>${utilisateur.username} - ${utilisateur.password}</li>
            </c:forEach>
        </ul>

    </div>

    <hr/>
</div>

<form method="post">
    <div>
        <label>
            Username
        </label>
        <input type="text" name="username" placeholder="Username">
    </div>

    <div>
        <label>
            Password
        </label>
        <input type="password" name="password" placeholder="Password"/>
    </div>

    <div>
        <label>
            Role
        </label>

        <select name="role">
            <option value="Admin">Admin</option>
            <option value="Utilisateur">Utilisateur</option>
        </select>
    </div>
    <input type="submit" value="Envoyer !">
<br>
    <c:forEach items="${errors}" var="error">

        <span class="error">Erreur : ${error.message} </span><br>

    </c:forEach>

</form>
</body>
</html>
