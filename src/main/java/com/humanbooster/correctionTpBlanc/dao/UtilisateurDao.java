package com.humanbooster.correctionTpBlanc.dao;

import com.humanbooster.correctionTpBlanc.model.Utilisateur;

import java.util.List;

public interface UtilisateurDao {
    List getAll();
    Utilisateur create(Utilisateur utilisateur);
}
