package com.humanbooster.correctionTpBlanc.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "etudiant")
public class Etudiant {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nom", nullable = false)
    @NotNull(message = "Veuillez saisir un nom")
    @NotBlank(message = "Veuillez saisir les points à repartir")
    private String nom;

    @Column(name = "prenom", nullable = false)
    @NotNull(message = "Veuillez saisir un prénom")
    @NotBlank(message = "Veuillez saisir un prénom")
    private String prenom;

    @ManyToOne(optional = false)
    @JoinColumn(name = "promotion_id", referencedColumnName = "id")
    @NotNull(message = "Veuillez saisir une promotion")
    @NotBlank(message = "Veuillez saisir une promotion")
    private Promotion promotion;

    public Etudiant() {
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public Integer getId() {
        return id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


}
