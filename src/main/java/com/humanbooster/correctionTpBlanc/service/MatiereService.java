package com.humanbooster.correctionTpBlanc.service;

import com.humanbooster.correctionTpBlanc.model.CritereEvaluation;
import com.humanbooster.correctionTpBlanc.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

public class MatiereService {

    private SessionFactory sessionFactory;

    public MatiereService() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public List getCriteresEvaluationByMatiere(Integer matiereId) {
        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();

        String hql = "SELECT ce FROM Matiere m LEFT JOIN m.criteresEvaluation ce WHERE m.id = :id";

        Query query = session.createQuery(hql);
        query.setParameter("id", matiereId);

        List criteresEvaluations = query.getResultList();

        session.close();

        return criteresEvaluations;
    }
}
