package com.humanbooster.correctionTpBlanc.service;

import com.humanbooster.correctionTpBlanc.dao.UtilisateurDao;
import com.humanbooster.correctionTpBlanc.model.Utilisateur;
import com.humanbooster.correctionTpBlanc.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

public class UserService implements UtilisateurDao {

    private SessionFactory sessionFactory;

    public UserService() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public List getAll() {
        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();

        String hql = "FROM Utilisateur u";

        Query query = session.createQuery(hql);

        List utilisateurs = query.getResultList();

        session.close();

        return utilisateurs;
    }

    @Override
    public Utilisateur create(Utilisateur utilisateur) {
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(utilisateur);
        session.flush();
        session.close();

        return utilisateur;
    }


}
