package com.humanbooster.correctionTpBlanc.service;

import com.humanbooster.correctionTpBlanc.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EtudiantService {

    private SessionFactory sessionFactory;

    public EtudiantService() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public Long countEtudiant() {
        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();

        String hql = "SELECT COUNT(e) FROM Etudiant e";

        Query query = session.createQuery(hql);

        Long nbEtudiants = (Long) query.getSingleResult();

        session.close();

        return nbEtudiants;
    }

    public List getEtudiantsForThisYear(Integer year){
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        String hql = "SELECT e FROM Etudiant e LEFT JOIN e.promotion promotion WHERE promotion.annee = :annee";

        Query query = session.createQuery(hql);
        query.setParameter("annee", year);

        List etudiants = query.getResultList();

        session.close();

        return etudiants;
    }

}
