package com.humanbooster.correctionTpBlanc.servlet;

import com.humanbooster.correctionTpBlanc.model.Etudiant;
import com.humanbooster.correctionTpBlanc.model.Utilisateur;
import com.humanbooster.correctionTpBlanc.service.EtudiantService;
import com.humanbooster.correctionTpBlanc.service.MatiereService;
import com.humanbooster.correctionTpBlanc.service.UserService;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class DashboardServlet extends HttpServlet {

    private UserService userService;
    private EtudiantService etudiantService;
    private MatiereService matiereService;

    public DashboardServlet() {
            super();
            this.userService = new UserService();
            this.etudiantService = new EtudiantService();
            this.matiereService = new MatiereService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Utilisateur> utilisateurs = (List<Utilisateur>) this.userService.getAll();


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Integer year = calendar.get(Calendar.YEAR);
        List currentEtudiants = this.etudiantService.getEtudiantsForThisYear(year);

        req.setAttribute("year", year);
        req.setAttribute("etudiantsYear", currentEtudiants);

        req.setAttribute("criteres", this.matiereService.getCriteresEvaluationByMatiere(1));
        req.setAttribute("utilisateurs", utilisateurs);
        req.setAttribute("nbEtudiants", this.etudiantService.countEtudiant());

        req.getRequestDispatcher("user-list.jsp").forward(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String username = req.getParameter("username");

        Utilisateur utilisateur = new Utilisateur(username, role, password);

        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();

        Set<ConstraintViolation<Utilisateur>> errors = validator.validate(utilisateur);

        if(errors.size() == 0){
            this.userService.create(utilisateur);
            resp.sendRedirect("/utilisateurs");
        } else {
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("user-list.jsp").forward(req, resp);
        }

    }
}
